<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'TaskController@index');

Route::get('/check', 'TaskController@checkdb');


Route::post('/of','TaskController@createOf');
Route::post('/pdf','TaskController@createPDF');
Route::get('/pdf/{hash}','TaskController@view');

