@extends('layout')




@section('content')


    <form action="/pdf" method="post" >
        @csrf

        @foreach ($tasks as $t)

        <div class="form-row">
            <div class="form-group col-md-2">
                <label for="{{ $t->id }}">Identifiant</label>
                <input type="text" value="{{ $t->id }}" class="form-control" name="id[{{ $t->id }}]" placeholder="" readonly>
            </div>

            <div class="form-group col-md-10">
                <label for="name{{ $t->id }}">Nom</label>
                <input type="text" value="{{ $t->name }}" class="form-control" name="name[{{ $t->id }}]" placeholder="" readonly>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-3">
                <label for="duavant{{ $t->id }}">Dû avant</label>
                <input type="text" value="{{ $t->due_date }}" class="form-control" name="duavant[{{ $t->id }}]" placeholder="" readonly>
            </div>

        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="description{{ $t->id }}">Description</label>
                <textarea id="desc{{ $t->id }} "class='ckeditor' class="form-control" name="description[{{ $t->id }}]" rows="6">{{ $t->description }}</textarea>
            </div>
        </div>

        @if ($t != end($tasks))
                <hr class="mb-4" style="height:30px; border:0px none; border-top:5px  ;background: #fff url(/img/hr-3.gif) repeat-x center center;">
        @endif


        @endforeach


        <hr class="mb-4">
        <button class="btn btn-primary btn-lg btn-block" type="submit">Continue</button>
    </form>

@endsection




