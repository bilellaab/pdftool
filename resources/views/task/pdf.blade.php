<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    @include('task.bootstrap')
    <style>
        div.c {
            font-size: 100%;
        }
    </style>
</head>
<body>

<div class="container">

    @foreach ($data as $d)

        <div class="row">
            <div class="col-12">


                        <div class="row p-5">
                            <div class="col-md-6 ">
                                <p class="text-dark text-left">  <h3>{{$d['name']}}</h3></p>
                                <p class="font-weight-bold mb-1 text-right">Cmd: #{{$d['id']}}</p>
                                <p class="text-muted text-right">Due to: {{$d['duavant']}}</p>
                            </div>
                        </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 c">

                {!! $d['description']  !!}

            </div>
        </div>
        @if ( $d != end($data))
        <div style="page-break-after: always;"></div>
        @endif
    @endforeach
</div>
</body>
</html>