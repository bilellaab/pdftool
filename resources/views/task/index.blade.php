@extends('layout')




@section('content')


    <form action="/of" method="post" >
        @csrf

        @foreach ($tasks as $t)
        <div class="custom-control custom-checkbox">
            <input name ="tasks[]"  value="{{ $t->id }}" type="checkbox" class="checkbox custom-control-input" id="{{ $t->id }}">
            <label class="custom-control-label" for="{{ $t->id }}">{{ $t->id }} - {{ $t->name }} -- <a href="http://vps599439.ovh.net/index.php/tasksComments?projects_id=8&tasks_id={{ $t->id }}" target="_blank">Link</a></label>
        </div>
        @endforeach
        <button id="check_all" type="button" class="btn btn-link" >Select all</button> / <button id="uncheck_all" type="button" class="btn btn-link" >Unselect all</button>
        <hr class="mb-4">
        <button class="btn btn-primary btn-lg btn-block" type="submit">Continue</button>
    </form>

@endsection