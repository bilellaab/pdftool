<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use NahidulHasan\Html2pdf\Facades\Pdf;
use Illuminate\Support\Facades\Crypt;

class TaskController extends Controller
{
    public function checkdb()
    {
        try {
            echo 'start testing...';
            $confirmedTasks = DB::select('SELECT * FROM `tasks` WHERE `projects_id` = ? AND `tasks_status_id` = ?', [8,1]);
            dd($confirmedTasks);

        } catch (\Exception $e) {
            die("Could not connect to the database.  Please check your configuration. error:" . $e );
        }
}


public function index()
{
    try {
        $confirmedTasks = DB::select('SELECT * FROM `tasks` WHERE `projects_id` = ? AND `tasks_status_id` = ?', [8,1]);
        //$confirmedTasks = ['id'=>1,"name"=>"abc"];
        return view('task.index', ['tasks' => $confirmedTasks]);

    } catch (\Exception $e) {
        die("Could not connect to the database.  Please check your configuration. error:" . $e );
    }
}

public function createOf(Request $request)
{   //Ordre de fabrication
    $ids = $request->all();
    //dd($ids);
    $ids=array_values($ids['tasks']);
    //dd($ids);
    $data = DB::table('tasks')->whereIn('id',$ids)->get();
    //dd($data);
    return view('task.of', ['tasks' => $data]);


}

    public function createPDF(Request $request)
{
    $post = $request->all();
    $data = [];
    foreach ($post['id'] as $id)
    {
        $data [$id]['id'] = $post['id'][$id];
        $data [$id]['name'] = $post['name'][$id];
        $data [$id]['description'] = $post['description'][$id];
        $data [$id]['duavant'] = $post['duavant'][$id];
    }

    \PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
    $pdf = \PDF::loadView('task.pdf',compact('data'));
    return $pdf->stream();

 }



    public function view($hash=null)
    {
        //$encrypted = Crypt::encryptString('Hello world.');
        //echo $encrypted;
        //get the id from the hash
        //$id = Crypt::decryptString($hash);
        //$id = $hash/32;
        $id = $hash;
        $d = DB::select('SELECT * FROM `tasks` WHERE `id` = ?', [$id]);
        $d = $d[0];
        $pdf = \PDF::loadView('task.pdfOne',compact('d'));
        return $pdf->stream();

    }


}